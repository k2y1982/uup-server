UUP转储开发服务器
---------------------------

### 描述
用于在Windows上设置UUP转储开发环境的工具
就像提取归档文件并运行几个脚本一样简单。

### 如何使用？
第一次运行时，需要运行 `update_xxxx.cmd`，自动下载UUP转储的最新版本及其最新依赖项。

要运行服务器，只需运行 `run.cmd`。它会自动打开你的默认浏览器。

服务器需要微软Visual C++ 2017可再发行，安装后即可工作。

要重新检索具有最新依赖项的最新UUP转储，请运行`update_xxxx.cmd`。

如果PHP由于某种原因无法工作，请查看控制台窗口以了解详细信息。

#

### 本项目中使用的项目
- aria2 (https://aria2.github.io/)
- PHP (http://php.net/)
- 7-Zip (https://www.7-zip.org/)

#

## 关于update_xxxx.cmd里5个下载网址的说明
- https://pd.zwc365.com/seturl/ 为镜像网站加速，如果网址失效，请自行替换当前可用网址。
```
rem website-master
echo https://pd.zwc365.com/seturl/https://github.com/uup-dump/website/archive/master.zip>"%scriptDir%\%tmpfile%"
echo   out=website-master.zip>>"%scriptDir%\%tmpfile%"
echo.>>"%scriptDir%\%tmpfile%"
```

## 更新说明 2-16
- 修正了由于 css js 缺失导致的页面样式错误
- 修正了`updeta_xxxx.com` 中一些无伤大雅的语法错误
- `files` 中的 7za.exe 更新至 v21.7 , aria2c.exe 更新至 v1.3.6.0
- `autodl_files` 中 7zr.exe 更新至 v21.7 , aria2c.exe 更新至 v1.3.6.0
- 删除了`update_1_xxxx.cmd` 里 关于 `json-api` 的命令行，对我们没用，还占地方 有 3GB+ 大啊！
- 如果想用 `PHP8.1.2`，请将 `/src/api/get.php` 中第`298`行 注释掉，不然 `str_replace()` 会报错（在最前面加 `//` 就OK了）
